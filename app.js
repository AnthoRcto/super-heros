// ###################################################
// DECLARATION VARIABLES
//####################################################
// Chemin superHeros.json
const url = "superHeros.json";
// Select
let form = document.querySelector(".formulaire .form-control");
let listHeroName = document.getElementById("superHerosName");
let listPublisher = document.getElementById("superHerosPublisher");
let classSelectHero = document.querySelector(".hero");
let classSelectPublisher = document.querySelector(".pub");
tabNom = [];
tabPublisher = [];
// Loader
const spinner = document.querySelector(".spinner");
// Cards
let displayHeros = document.getElementById("display-heroes");
let card = createNode("div");
let imageHero = createNode("img");
let titreNom = createNode("h2");
// Modale
let mainDisplay = document.querySelector(".display-modale");
let titreNomModale = createNode("h2");
let containerModale = createNode("div");
let modale = createNode("div");
let imageHeroModale = createNode("img");
let displayPowerstats = createNode("ul");
let liInt = createNode("li");
let liStrength = createNode("li");
let liSpeed = createNode("li");
let liDurab = createNode("li");
let liPower = createNode("li");
let liCombat = createNode("li");
// ###################################################
// FONCTIONS
//####################################################
/**
 * Fonction pour créer un élément dans le DOM
 */
function createNode(element) {
  return document.createElement(element);
}
/**
 * Fontion pour lier des éléments entre eux
 */
function createAppend(parent, element) {
  return parent.appendChild(element);
}
/**
 * Afficher les éléments d'une liste
 */
function displayList(liste, valeur) {
  for (i in liste) {
    let thisList = document.createElement("li");
    thisList.innerHTML = valeur;
  }
}
/**
 * fonction pour effacer les doublons
 */
function testDouble(array, string) {
  if (string != "" && string != null && array.indexOf(string) == -1) {
    array.push(string);
  }
}
/**
 * Fonction pour créer une modale
 */
function createModale(image,hero, int,strength,durability,speed,power,combat) {
  containerModale;
  modale;
  imageHeroModale;
  displayPowerstats;
  titreNomModale;
  liInt;
  liStrength;
  liSpeed;
  liDurab;
  liPower;
  liCombat;
  titreNomModale;
  createAppend(mainDisplay, containerModale);
  createAppend(containerModale, modale);
  createAppend(modale, imageHeroModale);
  createAppend(modale, displayPowerstats);
  createAppend(displayPowerstats, titreNomModale);
  createAppend(displayPowerstats, liInt);
  createAppend(displayPowerstats, liStrength);
  createAppend(displayPowerstats, liSpeed);
  createAppend(displayPowerstats, liDurab);
  createAppend(displayPowerstats, liPower);
  createAppend(displayPowerstats, liCombat);
  containerModale.classList.add("container-modale");
  modale.classList.add("modale");
  imageHeroModale.setAttribute("src", `${image}`);
  titreNomModale.innerHTML = `${hero}`;
  liInt.innerHTML = `Intelligence : ${JSON.stringify(int)}`;
  liStrength.innerHTML = `Strength : ${JSON.stringify(strength)}`;
  liSpeed.innerHTML = `Speed : ${JSON.stringify(speed)}`;
  liDurab.innerHTML = `Durability : ${JSON.stringify(durability)}`;
  liPower.innerHTML = `Power : ${JSON.stringify(power)}`;
  liCombat.innerHTML = `Combat : ${JSON.stringify(combat)}`;
}
/**
 * Fonction pour effacer la modale
*/
function eraseModale() {
  containerModale.remove();
  modale.remove();
  imageHeroModale.remove();
  displayPowerstats.remove();
  titreNomModale.remove();
}
/**
 * Fonction chargement page
*/
function loading() {
  window.addEventListener("load", () => {
    spinner.style.display = "none";
  });
}
// Quand la fenêtre est charger disparition du load
loading();
// ###################################################
// REQUÊTE API
//####################################################
fetch(url)
// Conversion des données récoltées au format Json
.then((response) => response.json())
.then((data) => {
  // Chemin vers toutes les données
  let superHeros = data.supersHeros;
  // Parcours toutes les données
  superHeros.map((superHero) => {
    let nomHero = superHero.name;
    let publisher = superHero.biography.publisher;
    // effacer les doublons des select
    testDouble(tabPublisher, publisher);
    testDouble(tabNom, nomHero);
    // Affiche les cards (image && nom) par défaut
    let imagesLargesHeros = superHero.images.lg;
    let card = createNode("div");
    let imageHero = createNode("img");
    let titreNom = createNode("h2");
    createAppend(displayHeros, card);
    createAppend(card, imageHero);
    createAppend(card, titreNom);
    card.classList.add("card-body");
    imageHero.setAttribute("src", `${imagesLargesHeros}`);
    titreNom.innerHTML = `${nomHero}`;
    // Affiche les cards selon le choix de l'éditeur
    // (https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement/change_event) => Permet de rentrer dans la function suite à un changement de sélection dans le <select>
    classSelectPublisher.addEventListener("change", (e) => {
      // Efface les cards précédemment affichées
      card.remove();
      if (publisher == e.target.value) {
        // Désactiver la sélection d'un héro
        listHeroName.setAttribute("disabled", "");
        card;
        imageHero;
        titreNom;
        createAppend(displayHeros, card);
        createAppend(card, imageHero);
        createAppend(card, titreNom);
        card.classList.add("card-body");
        imageHero.setAttribute("src", `${imagesLargesHeros}`);
        titreNom.innerHTML = `${nomHero}`;
      }
      if (classSelectPublisher.value == "all") {
        // Réactiver la sélection d'un héro
        listHeroName.removeAttribute("disabled", "");
        card;
        imageHero;
        titreNom;
        createAppend(displayHeros, card);
        createAppend(card, imageHero);
        createAppend(card, titreNom);
        card.classList.add("card-body");
        imageHero.setAttribute("src", `${imagesLargesHeros}`);
        titreNom.innerHTML = `${nomHero}`;
      }
    });
    
    // afficher la fenêtre modale en fonction de la valeur du select : "nomHero"
    listHeroName.addEventListener("change", (e) => {
      // Récolter les données du héro sélectionné
      if (nomHero == e.target.value) {
        eraseModale();
        // Désactiver la sélection d'un éditeur
        listPublisher.setAttribute("disabled", "");
        // Créer la fenêtre modale
        let powerstats = superHero.powerstats;
        // fonction qui modelise la modale
        createModale(
          imagesLargesHeros,
          nomHero,
          powerstats.intelligence,
          powerstats.strength,
          powerstats.durability,
          powerstats.speed,
          powerstats.power,
          powerstats.combat
          );
        }
        // Le cas où l'on sélectionne tous les heros
        if (classSelectHero.value === "all") {
          // Réactiver la sélection d'un héro
          listPublisher.removeAttribute("disabled", "");
            eraseModale();
            
          }
        });
      });
      
      // Remplissage du premier <select> : noms
      for (let i in tabNom) {
        tabNom.sort();
        let optionName = createNode("option");
        createAppend(listHeroName, optionName);
        optionName.setAttribute("value", `${tabNom[i]}`);
        optionName.innerHTML = `${tabNom[i]}`;
      }
      // Remplissage du deuxième <select> : éditeurs
      for (let i in tabPublisher) {
        tabPublisher.sort();
        let optionPublisher = createNode("option");
        createAppend(listPublisher, optionPublisher);
        // Donner une valeur à mes options
        optionPublisher.setAttribute("value", `${tabPublisher[i]}`);
        optionPublisher.innerHTML = `${tabPublisher[i]}`;
      }
    })
    .catch((error) => {
      console.log("error", error);
    });
  
    
    